#include <iostream>
#include "benchmarker.hpp"

int main(){
  Benchmarker bench;

  bench.start("Analysis of user input");

  std::string input;

  std::cin >> input;
  bench.checkpoint("Parse input");
  std::cin >> input;
  bench.checkpoint("Analyze data");
  
  std::cin >> input;
  bench.checkpoint("Log analyzed data");

  bench.end()->display();
  bench.display();

  return 0;
}